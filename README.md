# WEBToolBOX

![version](https://img.shields.io/badge/Ver-0.0.2-blue?style=for-the-badge) ![Gitee](https://img.shields.io/badge/Gitee-https%3A%2F%2Fgitee.com%2FZiChen__3317-red?style=for-the-badge)

#### 介绍

WEBToolBOX 是基于streamlit模块开发的网页工具箱

#### 软件架构

<br/>

#### 安装教程

##### 1.直接下载仓库内代码，运行RUN.bat，启动服务器（请确保你的设备已经安装Python3以及其他必须模块）

#### 使用说明

1. 该工具箱仅供学习，请勿用于商业牟利

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
